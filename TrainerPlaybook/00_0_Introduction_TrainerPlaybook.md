# Trainer Playbook - icAgile Programming - Introduction

## Introduction

This course is a combination of two ICAgile courses: Agile Programming and Agile Design. The modules have been drawn from both and have been arranged to provide a more holistic approach to the combined topics.

This playbook lists each of the sessions to be covered in the course. Although they have been listed in a particular order, feel free to rearrange the order if you feel it is appropriate. Furthermore, feel free to include additional sessions or omit as you see fit.

Each session has been described in terms taken from the book:  Training from the back of the room!. I.e., it is presumed that you will structure each session around the 4Cs: Connections, Concepts, Concrete Practice, and Conclusions.

![Training from the Back of the Room! - Sharon L. Bowman](../images/TFTBOTR.png)

This approach is becoming increasingly popular and is the preferred approach when designing Agile training courses and workshops.

The suggestions contained in this playbook are precisely that. Suggestions.

If you have alternative exercises or running order, then please go ahead and use them. The most important thing is that the objectives of the course are achieved and that the students leave the class feeling that they have learned something and that they have enjoyed the experience.

---

## General Ideas

Respect the timebox. When taking breaks and pairing off for exercises, we will resume at the agreed time rather than wait for people to rejoin. This is important. 

Terms of reference exercise (see fundamentals course). Have them come up with their own list. Such as punctuality (can also reference timeboxing), use of mobile phones in class, emails etc.

Guilty pleasures list. Invite them to contribute a title to the track list. Tunes to be played during breaks and breakouts. Hang on the wall.

---

## Lessons Learned - at the End of Each module

After each module, learners should be asked to complete the LessonsLearned document, which can be found in the `qaicpasd` repo.  They should branch this repo with their name as the branch, add to the document and then commit and push it.  This will provide a record to refer to when confirming certification at the end of the course.

---

## Activities for Introduction

## 1. Welcome

Display file: [00_0_CourseIntroduction.md](../DisplayFiles/00_0_CourseIntroduction.md)
  
- Welcome, domestics, AFA/virtual issues, icAgile background

  Emphasise that this isn’t a programming course.
  We are not going to help them with syntax or debug etc. That’s on them.

*Time box:* **20 minutes**

## 2. Introductions

- Pair learners,have them interview one another
  - why are they here? , 
  - what do they want to know? (See [Introductions](../DisplayFiles/00_1_Introductions.md))

If working virtually, set up break-out rooms with a 5-10 minute time box.

*Time box:* **30 minutes**

## 3. Set Up

See the guide called [00_2_SetUp.md](./00_2_SetUp.md) for learner set up instructions.

*Time box:* **20 minutes**

### Notes

- Provide learners with LOD or GTMPC links if necessary
- Assign all to pairs
- If 100% virtual, use break-out rooms; else pair learners in classroom

---

## Trainer Observations

Please list any observations or updates that you have from this section.

- The repos do not clone on Windows machines due to the file names being too long...(Press enter at the end of this line to continue)

Don't forget to commit changes and push!

[Next - Version Control](./01_VersionControl_TrainerPlaybook.md)
