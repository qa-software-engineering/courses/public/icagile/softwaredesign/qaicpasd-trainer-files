# Trainer Playbook - icAgile Software Design - Design for Automated Testing

## General

N/A

---

## Connections - Design for Automated Testing Part 1

**Question:** What are the benefits of automating testing?

Should answer: repeatable, consistent, automatic, can be part of build pipeline

**Question:** What tools may be used to test the User Interface?

Responses might include: Selenium, Katalon Studio, Unified Function Testing – UFT, TestComplete, Watir, IBM Rational Functional Tester, Ranorex

## Connections - Design for Automated Testing Part 2

**Question:** What patterns enable tests that bypass the User Interface and how?

Should answer: Snapshots, headless browser testing, e2e

## Connections - Design for Automated Testing Part 3

**Question:** What non-functional requirements NFRs can you identify?

Responses might include: Performance, response time, security, concurrent requests, transactions per second, response times, sub-service failures, Load, Quality attributes, etc

**Question:** What are the risks of delaying NFR testing until late in development?

Responses might include: Big job to include any of the above; may require large rewriting/refactoring of code to meet; architecture doesn't support performance requirements

---

## Concepts - Design for Automated Testing Part 1

Discuss UI testing tools and patterns

**Demo:**

- Show how calculator test works with [https://www.calculatorsoup.com/calculators/math/basic.php]
- Outline code with comments is in: [calculation.test.js](../Demos/06_DesignForAutomatedTesting/calculation.test.js)
- Completed code is in [calculation-complete.test.js](../Demos/06_DesignForAutomatedTesting/calculation-complete.test.js)

## Concepts - Design for Automated Testing - Part 2

Discuss answers supplied in the Connection question

## Concepts - Design for Automated Testing - Part 3

Discuss answers supplied in the Connection questions

Discuss how NFR testing can be incorporated into automated test runs using tools

### Please add notes below

---

## Concrete Practice - Design for Automated Testing

**Activity:** Coding

- Learners to complete a UI test of DuckDuckGo search page
  - can be found in `ConcretePractice/DesignForAutomatedCodeFiles/selenium`
- Learners to complete browserless testing of a rendering React application
  - can be found in `ConcretePractice/DesignForAutomatedCodeFiles/testing-without-browser`

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.

## Trainer Observations

Please list any observations or updates that you have from this section.

- First observation...(replace the text but not the \- for the bullet point in the md file here and add others as necessary)

Don't forget to commit changes and push!

[Previous - Continuous Delivery](./05_ContinousDelivery_TrainerPlaybook.md)[Next - Technical Debt](./07_TechnicalDebt_TrainerPlaybook.md)
