# Trainer Playbook - icAgile Programming - Day 2 Introduction

## Day Introduction

The purpose of this session is for learners to be aware of the programme for day 2.

---

## Activity 1 - Takeaways from yesterday

**Question:** - What were your main takeaways from yesterday?

## Activity 2 - Course Backlog Review

Display the course backlog and review progress.

| Item                                  | Status |
|---------------------------------------|--------|
| - Architecture                        | DONE   |
| - Design-In-The-Large                 | DONE   |
| - Simplicity - Simple and Good Design | DONE   |
| - Patterns (Design and Architecture)  | DONE   |
| - Design for Automated Testing        | TODO   |
| - Technical Leadership                | TODO   |
| - Continuous Delivery                 | TODO   |

---

## Trainer Observations

Please list any observations or updates that you have from this section.

- The repos do not clone on Windows machines due to the file names being too long...(Press enter at the end of this line to continue)

Don't forget to commit changes and push!

[Next - Continuous Delivery](./05_ContinuousDelivery_TrainerPlaybook.md)
