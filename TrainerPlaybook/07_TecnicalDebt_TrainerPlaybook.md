# Trainer Playbook - icAgile Software Design - Technical Debt

## General

N/A

---

## Connections - Technical Debt - Part 1

**Question:** How would you define Technical Debt?

Responses might include: Code that needs rework, untidy code, inefficient code, result of poor choices by developers.

## Connections - Technical Debt Part 2

**Question:** What are the symptoms of Technical Debt?
Messy code. Inefficient routine. Missed Sprint Goals, etc

**Activity:** What are the Pros and Cons of addressing Technical Debt?


Stakeholders may resist requests to address Technical Debt.
Technical Debt can limit your ability to deliver new features fast. This might be a strategic risk for your organization

**Activity:** Identify different approaches to reducing Technical Debt. For each, classify as reactive or proactive.

Responses might include: TDD, BDD, Periodic Refactoring, Pair Programming, 

---

## Concepts - Technical Debt Part 1

Discuss definitions of technical debt

## Concepts - Technical Debt - Part 2

Discuss symptoms of technical debt

### Please add notes below

---

## Concrete Practice - Technical Debt

Extension of Simplicity - Simple and Good Design Concrete Practice - explain how issues found could be remedied.

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.

## Trainer Observations

Please list any observations or updates that you have from this section.

- First observation...(replace the text but not the \- for the bullet point in the md file here and add others as necessary)

Don't forget to commit changes and push!

[Previous - Design for Automated Testing](./06_DesignForAutomatedTesting_TrainerPlaybook.md)[Next - Technical Leadership](./08_TechnicalLeadership_TrainerPlaybook.md)
