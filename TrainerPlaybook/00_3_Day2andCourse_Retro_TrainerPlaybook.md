# Trainer Playbook - icAgile Programming - Day 2 and Course Review & Retrospective

## Day 2 and Course Review & Retrospective

The purpose of this session is for learners to reflect on the day and the whole course.

---

## Activities for Day 2 Review & Retrospective

## 1. What was the stand out thing that they will take away from the course?

Display file: [00_5_Day2ReviewRetro.md](../DisplayFiles/00_5_Day2_ReviewRetro.md)
  
- Have learners write in their learning log
- Ask learners to share anything that they want to (no pressure or requirements to do so)

*Time box:* **10 minutes**

## 2. What feedback do they have for the course itself? This could be to do with timings, depth, materials, structure, exercises etc

- Trainer(s) will record this feedback and implement changes if possible the following day or following the course in time for future events

*Time box:* **10 minutes**

---

## Trainer Observations

Please list any observations or updates that you have from this section.

- The repos do not clone on Windows machines due to the file names being too long...(Press enter at the end of this line to continue)

Don't forget to commit changes and push!
