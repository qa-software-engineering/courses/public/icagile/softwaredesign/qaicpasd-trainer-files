# Trainer Playbook - icAgile Software Design - Continuous Delivery

## General

This section is large!

It is split into 5 sections - there are connections for 4 of them and discussion activities in 2 of them.

---

## Connections - Continuous Delivery Part 1

**Question:** How does Continuous delivery differ from Continuous integration?

Should answer: last mile

**Question:** What are the main benefits of Continuous Delivery?

Regular feedback, delivery of value etc. product enhancement

**Question:** What is the difference between Continuous Delivery and Continuous Deployment?

Should answer: Delivery is manually triggered. Usually by the customer.

- Hopefully they will come up with: MVC, MVVM, Layered, Microservices, etc.

**Quiz:** Which component of MVC contains…

---

## Concepts - Continuous Delivery Part 1

Discuss what Continuous Delivery is and how its defined

## Concepts - Continuous Delivery - Part 2

Need to look at the differences between Continuous Delivery and Continuous Deployment

## Concepts - Continuous Delivery - Part 3

Discuss knowledge of DevOps, required culture and concepts

## Concepts - Continuous Delivery - Part 4

Lead straight into concrete practice discussion - on both 3 ways and roles required

## Concepts - Continuous Delivery - Part 5

Look at the DevOps patterns and practices and lead into concrete practice discussion

### Please add notes below

---

## Concrete Practice - Continuous Delivery Part 4

**Activity:** Discussion -

>How does Continuous Delivery respond to and support each of The Three Ways?
>
>What new roles (and responsibilities) are needed for DevOps?

## Concrete Practice - Continuous Delivery Part 5

**Activity:** Discussion -

>What are the pitfalls (if any) of the following DevOps patterns?

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.

## Trainer Observations

Please list any observations or updates that you have from this section.

- First observation...(replace the text but not the \- for the bullet point in the md file here and add others as necessary)

Don't forget to commit changes and push!

[Previous - Day 1 Retro](./00_1_Day1_Retro_TrainerPlaybook.md)[Next - Design for Automated Testing](./06_DesignForAutomatedTesting_TrainerPlaybook.md)
