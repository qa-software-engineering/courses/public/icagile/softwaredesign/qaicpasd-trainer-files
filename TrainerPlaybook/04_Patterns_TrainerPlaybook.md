# Trainer Playbook - icAgile Software Design - Patterns (Design and Architecture)

## General

---

## Connections - Patterns

**Quiz:** Map design patterns to types?

**Question:** What different architecture patterns can you think of?

- Hopefully they will come up with: MVC, MVVM, Layered, Microservices, etc.

**Quiz:** Which component of MVC contains…

---

## Concepts - Patterns

Talk about design types. GoF.

Discuss which they recognise (if not by name). Which they might have used. Which they prefer and why.

Design Patterns do not  include SOLID as  SOLID is a set of Design “Principles”.

Talk about architecture patterns

**Demo:**  Factory pattern code. Features two types of computer:
Demos\04_Patterns\Java\FactoryPatterns

### Please add notes below

---

## Concrete Practice - Patterns - Factory Code to... and advantages and disadvantages of Architecture Patterns

**Exercise:** Patterns Concrete Practice

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.

## Trainer Observations

Please list any observations or updates that you have from this section.

- First observation...(replace the text but not the \- for the bullet point in the md file here and add others as necessary)

Don't forget to commit changes and push!

[Previous - Simple Design](./03_Architecture_TrainerPlaybook.md)[Next - Day 1 Retro](./00_1_Day1_Retro_TrainerPlaybook.md)
