# Trainer Playbook - icAgile Software Design - Technical Leadership

## General

N/A

---

## Connections - Technical Leadership - Part 1

**Question:** How would you define “Leadership”?

## Connections - Technical Leadership Part 2

**Question:** What are the responsibilities of a technical leader?

Possible responses: Knowing team members strengths and weaknesses, Mentoring team members, Saying, “Thank you” to the team, etc.

## Connections - Technical Leadership Part 3

**Question:** What effect does Agile have on the role of the leader?

Possible responses: self organised teams

## Connections - Technical Leadership Part 4

**Question:** What are the behaviours of a good technical leader?

## Connections - Technical Leadership Part 5

**Question:** What mistakes might a technical leader make?

---

## Concepts - Technical Leadership Part 1

Discuss definitions Leadership

## Concepts - Technical Leadership - Part 2

Discuss responses to question about responsibilities of a technical leader

## Concepts - Technical Leadership Part 3

Discuss Agile culture, etc with help of Concepts document

## Concepts - Technical Leadership - Part 4

Discuss responses to question about behaviours of a technical leader

## Concepts - Technical Leadership - Part 4

Discuss responses to question about mistakes technical leader may make.

### Please add notes below

---

## Concrete Practice - Technical Leadership

Signpost transcript of "I want to run an Agile project: Parts 1 & 2".

Ask learners to find examples of good and/or bad leadership within it and how they could improve the leadership shown.

Discuss.

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.

## Trainer Observations

Please list any observations or updates that you have from this section.

- First observation...(replace the text but not the \- for the bullet point in the md file here and add others as necessary)

Don't forget to commit changes and push!

[Previous - Technical Debt](./07_TechnicalDebt_TrainerPlaybook.md)[Next - Course Review/Retro](./00_3_Day2andCourse_ReviewRetro.md)
