# Trainer Playbook - icAgile Software Design - Architecture

## General

---

## Connections - Architecture

**Question:** How would you define Architecture?
**Question:** What impact does architecture have on a development team?
**Question:** What does architecture provide for development teams?

They may come back with analogies such as classic Architect role who is a senior developer former who sits in their office and draws out the architecture before handing it off to the team to follow. Much like a classic architect drawing up plans for builders to follow.
This is an incorrect impression. Teams by and large do this for themselves. And it also emerges over time.

---

## Concepts - Architecture

Acknowledge all valid points and point out any that they might have missed.

## Concrete Practice - Architecture

Scope requirements for cinema application using Zachman (or other)
### Please add notes below

Can mention Zachman and other.
Give the IEEE and ISO definitions.

Several techniques for capturing and modelling stakeholder needs. Each will suit differing viewpoints.
One common technique is Agile Requirements Modelling
There are a number of techniques for gathering and modelling stakeholder needs and viewpoints.
[http://agilemodeling.com/essays/agileRequirements.htm](http://agilemodeling.com/essays/agileRequirements.htm)

Listen to their ideas. and update and expand material over future deliveries.

Look at Ruth Malan later
[https://www.slideshare.net/RufM/visual-design-and-architecture](https://www.slideshare.net/RufM/visual-design-and-architecture)

#### IDEA

In future. Record an interview with stakeholder(s). They watch video then do some modelling.

**Roles:**
Enterprise architecture,  [system,town,city]
solutions architecture,  [buildings. Hospital, office block, station]
Technical architecture, [infrastructure, utilities, power, roads]

**Patterns**
n-tier
Microservices

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.

## Trainer Observations

Please list any observations or updates that you have from this section.

- First observation...(replace the text but not the \- for the bullet point in the md file here and add others as necessary)

Don't forget to commit changes and push!

[Previous - Introduction](./00_0_Introduction_TrainerPlaybook.md)[Next - Pair Programming](./02_Design-in-the-Large_TrainerPlaybook.md)
