# Trainer Playbook - icAgile Software Design - Simple Design

## General

---

## Connections - Simple Design

**Question:** What design principles and techniques can you think of?

- Activity suggests DRY to get them started.
- They may suggest YAGNI
- Hopefully they will mention SOLID
- KISS
- More is more complex MIMIC

**Question:** Under what circumstances might a principal such as DRY or SOLID conflict with simple design?

- Example duplicated code might be simple and readable but will conflict with DRY

**Activity:** Identify typical stages of an Agile lifecycle and design needs for each.

- Note: Delegate may not be able to identify a lifecycle or even think it’s relevant particularly in the context of CD

**Activity:** Apply YAGNI to technical work backlog.

---

## Concepts - Simple Design

Acknowledge all valid points and point out any that they might have missed.

Mention Martin Fowler YouTube video. It’s in the reading list.
Show them the various Ambler lifecycles.

Discuss prioritisation techniques.

They might mention MoSCoW but that simply give you a vocabulary. Mandated by DSDM.

SAFe recommends which has WSJF (Weighted Shorted Job First).

Variety of possible lifecycles. Whichever you follow, it will dictate when you design and to what extent.

### Please add notes below

---

## Concrete Practice - Bosun's chair

**Exercise:** Design-in-the-Large Concrete Practice

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.

## Trainer Observations

Please list any observations or updates that you have from this section.

- First observation...(replace the text but not the \- for the bullet point in the md file here and add others as necessary)

Don't forget to commit changes and push!

[Previous - Design-in-the-Large](./02_Design-in-the-Large_TrainerPlaybook.md)[Next - Patterns](./04_Patterns_TrainerPlaybook.md)
