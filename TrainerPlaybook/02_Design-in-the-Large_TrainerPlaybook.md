# Trainer Playbook - icAgile Software Design - Design-in-the-Large

## General

---

## Connections - Design-in-the-Large

**Question:** What are the pitfalls of too much upfront design?
**Activity:** List architectural decisions, whether they can be made early or late, and the impact of changing your mind later.

- e.g. Language, stack, platform, frameworks, libraries,  DB. If web app. Is it just a web app or cloud aware app. E.g. sit in AWS or use AWS. Domains.

**Activity:** Identify typical stages of an Agile lifecycle and design needs for each.

- Note: Delegate may not be able to identify a lifecycle or even think it’s relevant particularly in the context of CD

---

## Concepts - Design-in-the-Large

Acknowledge all valid points and point out any that they might have missed.

Mention Martin Fowler YouTube video. It’s in the reading list.
Show them the various Ambler lifecycles.

Discuss prioritisation techniques.

They might mention MoSCoW but that simply give you a vocabulary. Mandated by DSDM.

SAFe recommends which has WSJF (Weighted Shorted Job First).

Variety of possible lifecycles. Whichever you follow, it will dictate when you design and to what extent.

### Please add notes below

---

## Concrete Practice - Bosun's chair

**Exercise:** Design-in-the-Large Concrete Practice

Potential to skip Bosun's Chair in favour of Requirements Scoping (mandatory)

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.

## Trainer Observations

Please list any observations or updates that you have from this section.

- First observation...(replace the text but not the \- for the bullet point in the md file here and add others as necessary)

Don't forget to commit changes and push!

[Previous - Architecture](./01_Architecture_TrainerPlaybook.md)[Next - Simple Design](./03_SimpleDesign_TrainerPlaybook.md)
