require("chromedriver");
const { Builder, By, Key } = require("selenium-webdriver");

describe(`DuckDuckGo search string tests`, () => {

  const number1 = Math.random();
  const number2 = Math.random();
  let driver;

  beforeEach(async () => {

    try {
      // Get Selenium to open a Chrome browser
      driver = await new Builder().forBrowser("chrome").build();
    }
    catch {
      error => console.log(error);
    }
  })

  afterEach(async () => {
    // Clear the driver if it exists
    await driver?.quit();
  });

  test(`returns the correct value of 2 randomly generated numbers`, async () => {

    // Arrange 
    // Done in beforeEach()

    // Act
    // Get selenium to go to https://www.calculatorsoup.com/calculators/math/basic.php
    await driver.get("https://www.calculatorsoup.com/calculators/math/basic.php");

    // Get Selenium to click on the "4" button
    await driver.findElement(By.name("four")).click();

    // Get Selenium to click on the multiply button
    await driver.findElement(By.name("multiply")).click();

    // Get Selenium to the "5" button
    await driver.findElement(By.name("five")).click();

    // Get Selenium to click on the equals button
    await driver.findElement(By.name("calculate")).click();

    // Get Selenium to find the display
    const display = await driver.findElement(By.name("display"));

    // Assert
    // Expect the number displayed to match the multiplication of 4 and 5
    expect(await display.getAttribute("value")).toEqual("20");

    // Clean Up
    // Done in afterEach
  });
});
