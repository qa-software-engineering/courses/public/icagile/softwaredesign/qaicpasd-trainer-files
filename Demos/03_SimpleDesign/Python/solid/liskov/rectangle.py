import shape


class Rectangle(shape.Shape):

    def __init__(self, width, height):
        self.__width = width
        self.__height = height

    @property
    def width(self):
        return self.__width

    @width.setter
    def width(self, value):
        self.__width = value

    @property
    def height(self):
        return self.__width

    @height.setter
    def height(self, value):
        self.__height = value

    @property
    def area(self):
        return self.__width * self.__height


if __name__ == "__main__":
    # Comment out the area property and run
    c = Rectangle(2, 3);
    print(c.area)
