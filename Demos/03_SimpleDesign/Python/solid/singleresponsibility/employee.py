# New Employee class which has single responsibility
class Employee:
    def __init__(self):
        self.empId = None
        self.firstName = None
        self.lastName = None
        self.address = None
        self.__dateOfJoining = None
        self.__jobTitle = None
