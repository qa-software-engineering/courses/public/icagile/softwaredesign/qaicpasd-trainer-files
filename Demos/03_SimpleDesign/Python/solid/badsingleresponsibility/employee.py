# Breaks Single responsibility
# Should the Employee class have the responsibility of working out if promotion due this year?
# Also, should it calculate the income tax for the current year?


class Employee:

    def __init__(self):
        # we would have getters/setters for these
        self.empId = None
        self.firstName = None
        self.address = None
        self.dateOfJoining = None
        self.jobTitle = None

    @property
    def is_promotion_due_this_year(self):
        # logic to do promotion
        return True

    def calc_income_tax_for_current_year(self):
        # logic to do income tax
        return 0.0
