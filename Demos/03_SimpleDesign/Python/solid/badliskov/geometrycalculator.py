import math


# First try at Geometry calculator without Open/Closed principle


class GeometryCalculator:
    @classmethod
    def area(cls, shapes):

        area = 0
        for r in shapes:
            # Use duck typing not method overloading
            if hasattr(r, "radius"):
                area += math.pi * math.pow(r.radius, 2)
            elif hasattr(r, "width") and hasattr(r, "height"):
                area += r.width * r.height

        return area


if __name__ == "__main__":
    import circle as c, rectangle as rect

    shape1 = c.Circle(2)
    shape2 = rect.Rectangle(2, 3)

    shapes = [shape1, shape2]

    print(GeometryCalculator.area(shapes))
