// Breaks Single responsibility
// Should the Employee class have the responsibility of working out if promotion due this year?
// Also, should it calculate the income tax for the current year?
var Employee = /** @class */ (function () {
    function Employee() {
        this.empId = 0;
        this.firstName = "";
        this.address = "";
        this.dateOfJoining = null;
        this.jobTitle = "";
    }
    Object.defineProperty(Employee.prototype, "is_promotion_due_this_year", {
        get: function () {
            // logic to do promotion
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Employee.prototype, "calc_income_tax_for_current_year", {
        get: function () {
            // logic to do income tax
            return 0.0;
        },
        enumerable: true,
        configurable: true
    });
    return Employee;
}());
var c = new Employee();
console.dir(c);
