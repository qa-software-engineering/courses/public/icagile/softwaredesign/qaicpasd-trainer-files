// Breaks Single responsibility
// Should the Employee class have the responsibility of working out if promotion due this year?
// Also, should it calculate the income tax for the current year?


class Employee
{
    empId: Number;
    firstName: string;
    address: string;
    dateOfJoining: Date;
    jobTitle:string;
    constructor()
    {
        
        this.empId = 0
        this.firstName = ""
        this.address = ""
        this.dateOfJoining = null
        this.jobTitle = ""
    }
    
    get is_promotion_due_this_year():boolean
    {
        // logic to do promotion
        return true;
    }
    get calc_income_tax_for_current_year():Number
    {
        // logic to do income tax
        return 0.0;
    }
}

let c = new Employee();

console.dir(c)