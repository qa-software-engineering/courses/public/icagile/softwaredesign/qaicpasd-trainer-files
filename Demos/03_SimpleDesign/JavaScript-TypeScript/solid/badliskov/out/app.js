"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var geometrycalculator_1 = require("./geometrycalculator");
var circle_1 = require("./circle");
var rectangle_1 = require("./rectangle");
var totalArea = geometrycalculator_1.GeometryCalculator.area([new circle_1.Circle(5), new rectangle_1.Rectangle(2, 4)]);
console.log(totalArea);
