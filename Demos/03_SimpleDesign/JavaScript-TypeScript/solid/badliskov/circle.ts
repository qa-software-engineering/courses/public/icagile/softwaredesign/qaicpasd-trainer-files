

export class Circle
{

    private _radius: number;

    constructor(radius:number)
    {
        this.radius = radius;
    }
    
    get radius():number
    {
        return this._radius;
    }
    set radius(value:number)
    {
        this._radius = value;
    }
}




