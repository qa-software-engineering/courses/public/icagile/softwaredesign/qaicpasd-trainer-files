"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var circle_1 = require("./circle");
var rectangle_1 = require("./rectangle");
var geometrycalculator_1 = require("./geometrycalculator");
var shapes = [new circle_1.Circle(2), new rectangle_1.Rectangle(2, 3)];
var res = geometrycalculator_1.GeometryCalculator.area(shapes);
console.log(res);
