import {Shape} from "./shape"

export class GeometryCalculator
{
    
    static area(shapes:Array<Shape>):number
     {
        var area = 0;
        shapes.forEach(function(r)
        {
            area += r.area;
        });
        return area
     }

}