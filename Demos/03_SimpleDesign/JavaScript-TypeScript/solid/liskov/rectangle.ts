import {Shape} from "./shape"


export class Rectangle extends Shape
{

    private _width:number;
    private _height:number;

    constructor(width:number, height:number)
    {
        super();
        this._width = width
        this._height = height
    }
    
    get width():number
    {
        return this._width;
    }
    
    set width(value:number)
    {
        this._width = value;
    }
    
    get height():number
    {
        return this._width;
    }

    set height(value:number)
    {
        this._height = value;
    }
    get area()
    {
        return this._width * this._height;
    }

}