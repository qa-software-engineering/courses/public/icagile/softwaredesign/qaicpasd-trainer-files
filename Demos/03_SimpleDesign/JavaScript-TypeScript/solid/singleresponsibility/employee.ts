// New Employee class which has single responsibility
export class Employee
{
    private _empId:bigint;
    private _firstName:string;
    private _lastName:string;
    private _address : string;
    private _dateOfJoining: Date;
    private _jobTitle: string;

    constructor(first:string, last:string, address:string, joinDate:Date,title:string)
    {
        this._firstName = first;
        this._lastName = last;
        this._address = address;
        this._dateOfJoining = joinDate;
        this._jobTitle = title;

    }
}