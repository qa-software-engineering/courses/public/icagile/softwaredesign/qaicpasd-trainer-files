"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// New Employee class which has single responsibility
var Employee = /** @class */ (function () {
    function Employee(first, last, address, joinDate, title) {
        this._firstName = first;
        this._lastName = last;
        this._address = address;
        this._dateOfJoining = joinDate;
        this._jobTitle = title;
    }
    return Employee;
}());
exports.Employee = Employee;
