using System;
namespace SolidExample.BadLiskov
{
    public class GeometryCalculator {
        public double Area(Rectangle[] shapes) {
            double area = 0;
            foreach (Rectangle r in shapes) {
                area += r.getWidth() * r.getHeight();
            }
            return area;
        }

        public double Area(Circle[] shapes) {
            double area = 0;
            foreach (Circle r in shapes) {
                area += Math.PI * Math.Pow(r.getRadius(), 2);
            }
            return area;
        }

    }
}
