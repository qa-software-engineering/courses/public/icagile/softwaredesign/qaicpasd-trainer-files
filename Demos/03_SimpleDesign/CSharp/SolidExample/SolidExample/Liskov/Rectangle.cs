using System;
namespace SolidExample.Liskov
{

	public class Rectangle : Shape 
	{
	private double width, height;

	public double Height { get => Height1; set => Height1 = value; }
	public double Height1 { get => height; set => height = value; }

	public override double Area=> width * Height;
	
}
}