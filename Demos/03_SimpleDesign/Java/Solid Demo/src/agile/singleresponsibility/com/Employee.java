package agile.singleresponsibility.com;

import java.util.Date;

// New Employee class which has single responsibility
public class Employee {
	private String empId;
	private String firstName, lastName;
	private String address;
	private Date dateOfJoining;
	private String jobTitle;
}
