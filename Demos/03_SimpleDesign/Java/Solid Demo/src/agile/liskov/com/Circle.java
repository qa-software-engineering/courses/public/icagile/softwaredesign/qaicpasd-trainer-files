package agile.liskov.com;

public class Circle extends Shape {

	private double radius;
	
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}

	@Override
	public double Area() {
		return Math.PI * Math.pow(radius, 2);
	}

}
