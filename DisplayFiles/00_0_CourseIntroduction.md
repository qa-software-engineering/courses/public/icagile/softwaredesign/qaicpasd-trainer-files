# Agile Engineering

## icAgile - Agile Software Design

![This course is accredited by icAgile](../images/icAgileAccCourse.png)

---

## Course Delivery

![The 4 Cs](../images/CCCC.png)

![Training from the back of the room](../images/TFTBOTR.png)

---

## Health, safety and environment

![Health, safety and environment](../images/HSE.png)

Please pay attention to the standard health and safety briefing, as it is for your own benefit and that of your colleagues on the course. Also, please enjoy the lunches!

---

## icAgile Roadmap

ICAgile offers both knowledge-based and competency-based certifications:

- Certifications that start with “ICAgile Professional Certifications (ICP)” - Knowledge-based certifications focusing on fulfillment of ICAgile learning objectives and an in-class demonstration of acquired knowledge. (Pictured as silver ribbons)
- Certifications that start with “ICAgile Expert Certifications (ICE)” - Competency-based certification requiring considerable field experience and a demonstration of competency in front of an expert panel. (Pictured as gold ribbons)

![icAgile Learning Roadmap](../images/icAgileRoadmap.png)

---

## Course Backlog

- Architecture
- Design-In-The-Large
- Simplicity - Simple and Good Design
- Patterns (Design and Architecture)
- Continuous Delivery
- Design for Automated Testing
- Technical Leadership

---
