# Set Up the Working Environment

This is going to be the activity for the *Version Control Concrete Practice*

You are going to need several code bases to be able to fully take part in the activities in this course.  These have been provided for you in a repository.  After downloading and installing Git, dependent on your experience with Git, you will either:

- Clone repositories OR
- Download zipped version of the repositories and unzip them

To do this, go to the following address:

[https://gitlab.com/qa-software-engineering/courses/public/icagile/qaipasd.git]

Either:

- Use the **Clone** button to clone the repository here to a suitable place on your machine - you may also clone this on your own computer

OR

- Use the **Download** icon next to the Clone button to download a ZIP version of the repo and unzip it

---

This is the guide for the **Version Control** Concrete Practice - here for completeness with the correct repos in it.

## Cloning the Code Files Repository

Only *one of the pair* need do the following steps as you are going to be making your own version of this repo to share:

1. Go to the repository page at this address: [https://gitlab.com/qa-software-engineering/courses/public/icagile/qaipasd_code_files]
2. Using the command line (or GitBash) navigate to *C:/*.
3. In *gitlab*, click on the **Clone** button and copy the **HTTPS** link:

   ![GitLab Clone](../images/gitlab2.png)

4. Go back to your terminal and use the command:

   ```git clone https://gitlab.com/qa-software-engineering/courses/public/icagile/qaicpasd_code_files.git```

5. Wait for the repo to clone.
6. In your file explorer, navigate to the **agile-software-design** folder and remove the **.git** folder.

## Downloading the Zipped Repository

Only *one of the pair* need do the following steps - it can be skipped if the cloning steps have been taken

1. In *gitlab*, click on the **Download** button and then on **zip** - this should initiate download:
   ![GitLab Download](../images/gitlab3.png)
2. Extract the downloaded zip folder to *C:/*.
   - This will create a folder called agile-programming-master with the required files inside it.

## Create an account on a cloud-based git server

Only *the one of the pair* who cloned/unzipped should do this

1. Create a personal account on one of the following services (or use an existing account if you have one):

- [https://gitlab.com]
- [https://bitbucket.org]
- [https://github.com]

Please note that this is not an exhaustive and/or endorsed list!

## Create a shared repository for you and your partner

Only *the one of the pair* who cloned/unzipped should do this

1. In your terminal, navigate to the root folder for the repository you cloned/unzipped.
2. Initialise a **new** Git repository using the command ```git init```.
3. Stage all of the files in the repo using the command ```git add .```.
4. Commit the files with the command ```git commit -m "Initial commit"```.
5. In your cloud-based git service, create a new repository - it should be empty (not even a README.md document!).
6. Follow the steps shown to get the command to sync your cloud repo with the local one on your computer.  The command should be something like ```git remote add <someURL>```.
7. Push your local files to the remote repo using the command ```git push -u origin master```.
8. Either add your partner as an **owner/maintainer** of this repository or make it public so that they can clone and edit it.

## Get a copy of the new repository

The partner who has *not yet got a copy of the repo* should do this

1. Using the command line (or GitBash) navigate to *C:/*;
2. Use the ```git clone``` command with required URL to clone your partner's repository.

### Open your code base in an IDE

*Both in the pair* should do this:

The following IDEs are pre-installed on your computers for use with the code:

- VSCode
- PyCharm CE
- Visual Studio CE
- Eclipse
- IntelliJ

Using the whole repo as a project, open your code base in your preferred IDE.  If your preferred option is not in the list, feel free to download and install your own.

*Whole Activity Time Box:* **20 minutes**
