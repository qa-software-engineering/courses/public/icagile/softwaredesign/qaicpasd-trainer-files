# Day 2 Introduction

The purpose of this session is for learners to be aware of the programme for day 2.

## **Question:** What your takeaways from yesterday?

Refer to your learning log and answer in the medium given.

*Whole Activity Time Box:* **5 minutes**

## Course Backlog

| Item                                  | Status |
|---------------------------------------|--------|
| - Architecture                        | DONE   |
| - Design-In-The-Large                 | DONE   |
| - Simplicity - Simple and Good Design | DONE   |
| - Patterns (Design and Architecture)  | DONE   |
| - Continuous Delivery                 | TODO   |
| - Design for Automated Testing        | TODO   |
| - Technical Leadership                | TODO   |

*Whole Activity Time Box:* **5 minutes**
