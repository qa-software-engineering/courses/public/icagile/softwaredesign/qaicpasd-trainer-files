# Introductions

1. Split into pairs - if working virtually, help will be given to assign partners.
2. Interview each other to find out:

- Who the other person is;
- Why they are here;
- What they are wanting to learn from the course;
- An interesting fact about them.

You have a *timebox* of 3 minutes.
You will then introduce the other person to the whole group.

*Whole Activity Time Box:* **30 minutes**
