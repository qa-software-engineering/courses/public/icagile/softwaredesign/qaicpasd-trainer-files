# Day 2 and Course Review & Retrospective

The purpose of this session is for learners to reflect on the course.

## **Question:** What were the stand out things that you will take away from the course?

Write your thoughts in your learning log.

*Whole Activity Time Box:* **10 minutes**

## **Question:** Is anyone prepared to share?

*Whole Activity Time Box:* **10 minutes**

## **Question:** Do you have any feedback as a course stakeholder?

- This could be to do with timings, depth, materials, structure, exercises etc
- Please give feedback via the medium your trainer provides

*Whole Activity Time Box:* **10 minutes**
