# Collaborative Tools

Virtual Delivery will be through QA's WebEx Meetings.  This facilitates:

- Sharing of screens (both by trainers and by participants)
- Chat
- Polling and Q&A
- Breakout Rooms
- Annotating and whiteboards

## Connections Activities

Use [PollEv.com](https://pollev.com) for all questions and activities in Connections sessions

## Concrete Practice

### Coding Exercises

Learners are asked to clone/fork the code files repo found at:

[qaicpasd-code-files](https://gitlab.com/qa-software-engineering/courses/public/icagile/softwaredesign/qaicpasd-code-files)

Learners will be put into breakout rooms for pair programming and collaboration

Learners will be encouraged to share repo details and where appropriate, use VSCode's Live Share facility to code collaboratively.

### Other Exercises

Where pair/small group discussions are used, learners will be placed into Breakout Rooms to facilitate discussion and collaboration.

## Conclusions

Still working on an online solution for sharing Conclusions Learning Logs in real-time - at the moment, learners will need to share via email or via WebEx